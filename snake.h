// Nome completo do aluno:
// Data da última modificação:
#include <curses.h>
#include <stdlib.h> /* malloc */
#include <string.h> /* memcpy */
#include <stddef.h>
#include <time.h>

#ifndef SNAKE_H
#define SNAKE_H

// Uma snake eh formada por uma cadeia de segmentos
struct segmento {
    int x, y;   /// posicao
    char c;   // letra consumida 
    struct segmento* next;
};

void insereSegmentoFinal(struct segmento *m, char letrinha);
struct segmento* procuraSegmento(struct segmento* m, char letrinha);
void removePrimeiroSegmento(struct segmento* m);
int contaSegmentos(struct segmento* m);
void insereSegmentoInicio(struct segmento* m, char letrinha);
void insereSegmentoOrdenado(struct segmento* m, char letrinha);

#endif